from datetime import datetime
from app import db, login_manager
from flask_login import UserMixin


@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))

post_tags = db.Table('post_tags',db.Column('post_id',db.Integer,db.ForeignKey('post.id')),
                                 db.Column('tag_id',db.Integer,db.ForeignKey('tag.id')))

class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(20), unique=True, nullable=False)
    email = db.Column(db.String(120), unique=True, nullable=False)
    password = db.Column(db.String(60), nullable=False)
    posts = db.relationship('Post', backref='author', lazy=True)
   

    def __repr__(self):
        return f"User('{self.username}', '{self.email}')"


class Post(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(100), nullable=False)
    date_posted = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    content = db.Column(db.Text, nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    tags = db.relationship('Tag', secondary = post_tags, backref = db.backref('posts'), lazy = 'dynamic')

    def __repr__(self):
        return f"Post('{self.title}', '{self.content}', '{self.date_posted}')"

class Tag(db.Model):
    id = db.Column(db.Integer,primary_key=True)
    title = db.Column(db.String(140))

    def __repr__(self):
        return f"Tag('{self.id}', '{self.title}')"