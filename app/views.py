import os
import secrets
from flask import render_template, url_for, flash, redirect, request, abort, make_response
from app import app, db, bcrypt
from .forms import RegistrationForm, LoginForm, UpdateAccountForm, PostForm, resetPasswordForm
from .models import User, Post, Tag
from flask_login import login_user, current_user, logout_user, login_required
import logging


@app.route("/", methods=['GET', 'POST'])

@app.route("/home",  methods=['GET', 'POST'])
def home():
    posts = Post.query.all()
    app.logger.info('home route request.')
    if request.method == 'POST':
        res = make_response("")
        res.set_cookie("font", request.form.get('font'), 60*60*24*15)
        res.headers['location'] = url_for('home')
        return res, 302
    return render_template('home.html', posts=posts)


@app.route("/about")
def about():
    app.logger.info('about route request.')
    return render_template('about.html', title='About')

@app.route("/announcements")
def announcements():
    app.logger.info('announcements route request.')
    return render_template('announcements.html', title='Announcements')


@app.route("/register", methods=['GET', 'POST'])
def register():
    app.logger.info('register route request.')
    if current_user.is_authenticated:
        app.logger.debug('current user authenticated, current user: %s.',user.username)
        return redirect(url_for('home'))
    
    form = RegistrationForm()
    if form.validate_on_submit():
        hashed_password = bcrypt.generate_password_hash(form.password.data).decode('utf-8')
        user = User(username=form.username.data, email=form.email.data, password=hashed_password)
        db.session.add(user)
        db.session.commit()
        flash('Your account has been created! You are now able to log in', 'success')
        app.logger.debug('username %s.', user.username)
        app.logger.info('account created sucessfully.')
        return redirect(url_for('login'))
    return render_template('register.html', title='Register', form=form)


@app.route("/login", methods=['GET', 'POST'])
def login():
    app.logger.info('login page request.')
    
    if current_user.is_authenticated:
        return redirect(url_for('home'))
    
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        if user and bcrypt.check_password_hash(user.password, form.password.data):
            app.logger.info('%s logged in sucessfully.', user.username)
            login_user(user, remember=form.remember.data)
            return redirect(url_for('home'))
        else:
            flash('Login Unsuccessful. Please check email and password.', 'danger')
            app.logger.info('logged in failed.')
    return render_template('login.html', title='Login', form=form)

@app.route("/account", methods=['GET', 'POST'])
@login_required
def account():
    app.logger.info('account route request.')
    form = UpdateAccountForm()
    if form.validate_on_submit():
        current_user.username = form.username.data
        current_user.email = form.email.data
        current_user.password = bcrypt.generate_password_hash(form.new_password.data).decode('utf-8')
        db.session.commit()
        flash('Your account has been updated!', 'success')
        app.logger.info('account updated sucessfully.')
        return redirect(url_for('account'))
    
    elif request.method == 'GET':
        form.username.data = current_user.username
        form.email.data = current_user.email
        app.logger.info('GET account route request.')
    return render_template('account.html', title='Account', form=form)


@app.route("/forgot_password", methods=['GET','POST'])
def forgot_password():
    app.logger.info('forgot_password route request.')
    form = resetPasswordForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        if user:
            app.logger.debug('%s account has been found. The email is: %s.', user.username, user.email)
            user.password = bcrypt.generate_password_hash(form.password.data).decode('utf-8')
            db.session.commit()
            flash('Your account has been found and Your password has been reset!', 'success')
            return redirect(url_for('login'))
        else:
            flash('Cannot find your account! PLease check your e-mail address', 'danger')
            return redirect(url_for('forgot_password',title='Forgot password', form=form))
    return render_template('forgot_password.html',title='Forgot password',form=form)


@app.route("/logout")
def logout():
    logout_user()
    return redirect(url_for('home'))

@app.route("/post/new", methods=['GET', 'POST'])
@login_required
def new_post():
    app.logger.info('new post route request.')
    form = PostForm()
    if form.validate_on_submit():
        post = Post(title=form.title.data, content=form.content.data, author=current_user)
        db.session.add(post)
        tag = Tag(title=form.tags.data)
        tags = Tag.query.all()
        count = 0
        num_tags = len(tags)
        
        if not tag.title:
            db.session.commit()
            flash('Your post has been created without a tag!', 'success')
            app.logger.info('the post has been created without a tag.')
            return redirect(url_for('home'))
        else:
            # adding tag for the first time
            if not tags:
                db.session.add(tag)
                post.tags.append(tag)
                db.session.commit()
                flash('Your post has been created!', 'success')
                app.logger.debug('tag is %s.',tag.title)
                app.logger.info('the post has been created with a tag.')
                return redirect(url_for('home'))
            # make sure all tags appear only once in the database 
            else:
                for TAG in tags:
                    if TAG.title == tag.title:
                        post.tags.append(TAG)
                        db.session.commit()
                        flash('Your post has been created!', 'success')
                        app.logger.debug('tag is %s. TAG is %s',tag.title,TAG.title)
                        app.logger.info('the post has been created with a tag(tag found in db).')
                        return redirect(url_for('home'))
                    else:
                        count += 1
                if count == num_tags:
                    db.session.add(tag)
                    post.tags.append(tag)
                    db.session.commit()
                    flash('Your post has been created!', 'success')
                    app.logger.debug('tag is %s.',tag.title)
                    app.logger.info('the post has been created with a tag, new tag added to the database.')
                    return redirect(url_for('home'))
    return render_template('create_post.html', title='New Post',
                           form=form, legend='New Post')

# @app.route("/create_tags", methods=['GET', 'POST'])
# @login_required
# def create_tags():
#     form = TagForm()
#     if form.validate_on_submit():
#         tag = Tag(title=form.tags.data)
#         db.session.add(tag)
#         db.session.commit()
#         flash('Your post has been created!', 'success')
#     return render_template('create_tags.html', title='New Tag', 
#                             form=form, legend='Create Tags')

@app.route("/all_tags", methods=['GET','POST'])
@login_required
def all_tags():
    tags = Tag.query.all()
    app.logger.info('all tag page request.')
    return render_template('all_tags.html',tags=tags)

@app.route("/post/<int:post_id>")
def post(post_id):
    app.logger.info('post route request.')
    post = Post.query.get_or_404(post_id)
    app.logger.debug('post %i requested.',post_id)
    return render_template('post.html', title=post.title, post=post)

@app.route("/post/<int:post_id>/update", methods=['GET', 'POST'])
@login_required
def update_post(post_id):
    app.logger.info('update post route request.')
    post = Post.query.get_or_404(post_id)
    if post.author != current_user:
        app.logger.debug('not current user.')
        abort(403)
    form = PostForm()
    if form.validate_on_submit():
        post.title = form.title.data
        post.content = form.content.data
        
        tag = Tag(title = form.tags.data)
        
        current_tags = post.tags.all()
        num_tags = len(current_tags)
        count = 0
        if not tag.title:
            flash('Your post has been updated(no tag added)!', 'success')
            app.logger.debug('updating post %i without a tag.',post_id)
            db.session.commit()
            return redirect(url_for('post', post_id=post.id))
        else:
            db_tag = Tag.query.filter_by(title=form.tags.data).first()
            
            for TAG in current_tags:
                if TAG.title == tag.title:
                    flash('Your post has been updated(same tag will be ignored)!', 'success')
                    app.logger.debug('updating post %i with a same tag(tag ignored).',post_id)
                    return redirect(url_for('post', post_id=post.id)) 
                else:
                    count += 1
            # if the tag in db but not in current_tag
            if db_tag and count == num_tags:
                post.tags.append(db_tag)
                db.session.commit()
                flash('Your post has been updated!', 'success')
                app.logger.debug('updating post %i with a tag, the tag has been found in database.',post_id)
                return redirect(url_for('post', post_id=post.id))
            # if tag not in db and not in current_tag
            elif not db_tag and count == num_tags:
                db.session.add(tag)
                post.tags.append(tag)
                db.session.commit()
                flash('Your post has been updated(tag created)!', 'success')
                app.logger.debug('updating post %i with a tag, and the tag is added to the database.',post_id)
                return redirect(url_for('post', post_id=post.id))
            # for db_tag in db_tags:
            #     if db_tag.title != tag.title:
            #         db_count += 1
            # # if new tag isn't in db and isn't in current_tags, add it to the db and the post
            # if count == num_tags and db_count == num_db_tags:
            #     print('yes 1')
            #     db.session.add(tag)
            #     post.tags.append(tag)
            #     db.session.commit()
            #     flash('Your post has been updated(new tag added)!', 'success')
            #     return redirect(url_for('post', post_id=post.id))
            # elif db_count and count == num_tags:
            #     print('yes 2')
            #     db.session.commit()
            #     flash('Your post has been updated!', 'success')
            #     return redirect(url_for('post', post_id=post.id))
            # #db.session.commit()
    elif request.method == 'GET':
        form.title.data = post.title
        form.content.data = post.content
    return render_template('create_post.html', title='Update Post',
                           form=form, legend='Update Post')


@app.route("/post/<int:post_id>/delete", methods=['POST'])
@login_required
def delete_post(post_id):
    app.logger.info('deleting post.')
    post = Post.query.get_or_404(post_id)
    if post.author != current_user:
        abort(403)
    db.session.delete(post)
    db.session.commit()
    flash('Your post has been deleted!', 'success')
    app.logger.debug('%i post deleted.',post_id)
    return redirect(url_for('home'))
   

